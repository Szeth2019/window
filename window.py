
# This gives the bounds of the smallest window (first and last spot in the array) that needs to be sorted
# for example

class SmallestWindow:

    def __init__(self):
        print("in init")

    def selectionsort(self, arraypassed):

        # create real copy, not a reference
        array = arraypassed[:]

        for i in range(len(array)):
            # lowest value is i in this iteration
            lowest = i

            for j in range(i + 1, len(array)):
                if array[lowest] > array[j]:
                    lowest = j

            # put lowest value in the lowest spot
            temp = array[i]
            array[i] = array[lowest]
            array[lowest] = temp

        return array

    def window(self, array, sortedarray):
        lowest = -1
        highest = -1

        for i in range(len(array)):
            if array[i] != sortedarray[i] and lowest == -1:
                lowest = i
            elif sortedArray[i] != array[i]:
                highest = i

        return lowest, highest

# Try a few arrays to check my solution
print("Test 1: ")
arr = [3, 7, 5, 6, 9]
# create object
first = SmallestWindow()
# get the sorted array (uses selection sort)
sortedArray = first.selectionsort(arr)
# print both arrays
print("Sorted array:   " + str(sortedArray))
print("Unsorted array: " + str(arr))
# print the answer
print("The window that needed to be changed is: " + str(first.window(arr, sortedArray)))

print()

print("Test 2: ")
# array to check part 2 (the discount double check)
arr = [1, 2, 7, 8, 3, 7, 5, 6, 9, 1, 12]
# create object
second = SmallestWindow()
# get the sorted array (uses selection sort)
sortedArray = second.selectionsort(arr)
# print both arrays
print("Sorted array:   " + str(sortedArray))
print("Unsorted array: " + str(arr))
# print the answer
print("The window that needed to be changed is: " + str(second.window(arr, sortedArray)))
